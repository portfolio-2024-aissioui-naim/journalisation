__Journalisation en PHP - Naïm Aissioui__

Qu'est que la journalisation ?

La journalisation (ou logging en anglais) est le processus par lequel des événements, actions ou données sont enregistrés de manière chronologique dans un journal ou un fichier, généralement dans le but de faciliter le dépannage, la surveillance, l'analyse ou l'audit d'un système informatique ou d'une application.

Dans le domaine de l'informatique, la journalisation est largement utilisée dans le développement de logiciels, les systèmes d'exploitation, les réseaux informatiques, les bases de données et d'autres domaines. 

<br>
<br>
Voici les lignes de code à ajouter pour executer la journalisation ($info) :  
<br>
<br>
<br>

![code](annexe/Code_journalisation.png)

<br>
<br>
<br>
Les actions que nous pourrons faire ( ajouter, supprimer, sauvegarder et modifier) :  
<br>
<br>
<br>

![action](annexe/Action.png)

<br>
<br>
<br>
Où ce trouve la journalisation :  
<br>
<br>
<br>

![log](annexe/log.png)

